/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import FlashMessage from "react-native-flash-message";

import ROUTER from "@routes/router";



class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <ROUTER properties={this} />
        <FlashMessage
          position="bottom"
          ref={(component) => { this.flashMessage = component; }}
          duration={2000}
          titleStyle={[{ fontSize:16, color: "white" }]}
          textStyle={[{ fontSize: 14, color: "white", fontWeight: "600" }]}
        />
      </NavigationContainer>
    )
  }
}


export default App;
