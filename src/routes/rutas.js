import Home from '@pages/Home';
import Chat from '@pages/Chat';
import ChatClient from '@pages/ChatComplete/ChatClient';

export { Home, Chat, ChatClient }