import React from 'react';
import { View, Text } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


// Rutas
import * as RUTAS from './rutas'
const Stack = createStackNavigator();



function start(props) {
    return (
        <Stack.Navigator headerMode={"none"}>
            <Stack.Screen name="Home" component={RUTAS.Home} />
            <Stack.Screen name="Chat" component={RUTAS.Chat} />
            <Stack.Screen name="ChatClient" component={RUTAS.ChatClient}  lleve={props}/>
        </Stack.Navigator>
    );
}

export default start;