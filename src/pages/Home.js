/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useRef } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { Button, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { showMessage, hideMessage } from "react-native-flash-message";



function App(props) {
  // Declaración de una variable de estado que llamaremos "username"
  const [username, setUsername] = useState('');
  const inputUsername = useRef('inputUsername');



  return (
    <View style={ styles.viewContent }>
      <StatusBar barStyle="dark-content" />
                
          <View style={styles.body}>
            <View style={styles.container}>

              <Text style={styles.sectionTitle}>Step One</Text>
              <Text style={styles.sectionDescription}>
                Edit <Text style={styles.highlight}>App.js</Text> to change this
                screen and then come back to see your edits.
              </Text>

              <Input
                ref={inputUsername}
                value={username}
                containerStyle={[styles.containerBotton]}
                leftIcon={
                  <Icon
                    name='user'
                    size={24}
                    color='black'
                  />
                }
                onChangeText={value => setUsername(value) }
                placeholder='Ingrese su nombre de usaurio'
                errorStyle={{ color: 'red' }}
                errorMessage=''
              />

              <Button
                containerStyle={[styles.containerBotton]}
                title="Aceptar"
                onPress={() => {
                  name(props, username);
                }}
              />
            </View>

          </View>
       
       
     
    </View>
  )


}


function name(props, state) {

  if (state){
    props.navigation.navigate('ChatClient', { datos: state })
  }else{
    showMessage({
      message: "Alerta",
      description: "El campo nombre de usuario es requerido",
      type: "warning",
    });
  }
  
}




const styles = StyleSheet.create({
  viewContent: {
    flex: 1
  },
  body: {
    flex: 1,
    justifyContent:"center",
    backgroundColor: 'white',
  },
  container: {
    paddingHorizontal: '8%',
  },
  containerBotton: {
    marginTop: '10%',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },

});

export default App;
