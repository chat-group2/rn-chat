import React from 'react';
import Pusher from 'pusher-js/react-native';
import { StyleSheet, View, Text, KeyboardAvoidingView } from 'react-native';


import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import pusherConfig from '@constans/pusher';
import ChatView from './ChatView';



export default class ChatClient extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            messages: []
        };


        this.pusher = new Pusher(pusherConfig.key, pusherConfig); // (1) inicializar pusher con la configuracion desde pusher.js
        this.chatChannel = this.pusher.subscribe('chat_channel'); // (2) nos suscribimos al canal Pusher al que nuestro servidor agrega todos los mensajes


        this.chatChannel.bind('pusher:subscription_succeeded', () => { // (3) Esta es una devolución de llamada cuando la suscripción ha sido exitosa, ya que es un evento asincrónico

            this.chatChannel.bind('join', (data) => { // (4) 
                this.handleJoin(data.name);
            });

            this.chatChannel.bind('part', (data) => { // (5) 
                this.handlePart(data.name);
            });

            this.chatChannel.bind('message', (data) => { // (6) 
                this.handleMessage(data.name, data.message);
            });
        });

        this.handleSendMessage = this.onSendMessage.bind(this); // (9)
    }


    handleJoin(name) { // (4) cada vez que recibimos un joinmensaje en el canal y agrega un mensaje a nuestra lista.
        const messages = this.state.messages.slice();
        messages.push({ action: 'join', name: name });
        this.setState({
            messages: messages
        });
    }

    handlePart(name) { // (5) cada vez que recibimos un partmensaje en el canal y agrega un mensaje a nuestra lista.
        const messages = this.state.messages.slice();
        messages.push({ action: 'part', name: name });
        this.setState({
            messages: messages
        });
    }

    handleMessage(name, message) { // (6) cada vez que recibimos un messagemensaje en el canal y agrega un mensaje a nuestra lista.
        const messages = this.state.messages.slice();
        messages.push({ action: 'message', name: name, message: message });
        this.setState({
            messages: messages
        });
    }

    componentDidMount() { // (7) Cuando el componente se monta por primera vez, enviamos un mensaje al servidor informándoles del usuario que se ha conectado
        fetch(`${pusherConfig.restServer}/users/${this.props.route.params.datos}`, {
            method: 'PUT'
        }).then((response) => response).then((response) => {
            console.log("PUT: ", response);
        });
    }

    componentWillUnmount() { // (8) Cuando el componente se desmonta, enviamos un mensaje al servidor informándoles del usuario que se ha ido
        fetch(`${pusherConfig.restServer}/users/${this.props.route.params.datos}`, {
            method: 'DELETE'
        }).then((response) => response).then((response) => {
            console.log("Delete: ", response);
        });
    }

    onSendMessage(text) { // (9)  enviar un mensaje al servidor, que se conectará pronto
        const payload = {
            message: text
        };
        fetch(`${pusherConfig.restServer}/users/${this.props.route.params.datos}/messages`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        }).then((response) => response).then((response) => {
            console.log("POST: ", response);
        });
    }

    render() { // (10) Renderizamos nuestro chat
        const messages = this.state.messages;

        return (
            <View style={styles.container}>
                <Header
                    leftComponent={() => {
                        return (
                            <Icon
                                name="chevron-left"
                                size={16}
                                color="#fff"
                                onPress={() => {
                                    this.props.navigation.pop()
                                }}
                            />

                        )
                    }}
                    centerComponent={{ text: 'PUSHCER CHAT', style: { color: '#fff', fontWeight: "600" } }}
                />
                <ChatView messages={messages} onSendMessage={this.handleSendMessage} />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //alignItems: 'flex-start',
        //justifyContent: 'flex-start',
        //paddingTop: 50
    },
    
});