> Brandon Henao


## Advertencia de rendimiento
El despliegue se encuentra en un entorno gratuito y por tanto limitado. Pueden haber retrasos en las respuestas o peticiones http perdidas.

### Demo
En la imagen se demuestra como podemos empezar a usar nuestra aplicación.  

![demo](readme-assets/demo.gif)


## Herramientas
- Visual studio code
- Genymotion
- LICEcap


## Tecnologías
- React Native 0.63
- React Navigation
- Pusher
- native-base
- react-native-gifted-chat
- react-native-vector-icons
- react-native-elements
- rutas absolutas  

  npm install --save-dev babel-plugin-module-resolver

    {
    "plugins": [
        ["module-resolver", {
        "root": ["./src"],
        "alias": {
            "test": "./test",
            "underscore": "lodash"
        }
        }]
    ]
    }


## Comandos

    npx pod-install ios    
    npx react-native run-ios --simulator="iPhone 11"    